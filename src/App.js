import React, { Suspense } from 'react';
import './App.css';
import { Route, Routes } from 'react-router-dom';
import Home from './component/Home';
import About from './component/About';
import Header from './component/Header';
import Product from './component/Product';
import Footer from './component/Footer';
import ProductDetails from './component/ProductDetails'
import Cart from './component/Cart';
import Registration from './component/Registration'
import Login from './component/Login';
import Payment from './component/Payment';
import EmpListing from './component/EmpListing';
import EmpCreate from './component/EmpCreate';
import EmpDetail from './component/EmpDetail';
import EmpEdit from './component/EmpEdit';
import { ToastContainer } from 'react-toastify';
const LazyAbout = React.lazy(() => import('./component/Product'))

function App() {
  return (
    <>
    <ToastContainer theme='colored' position='top-center'/>
      <Header />
      <Routes>
        <Route path="/Home" element={<Home />} />
        <Route path="About" element={<About />} />
        {/* <Route path="Product" element={<Product />}></Route> */}
        <Route path="Product" element={
          <Suspense fallback={<div>Please Wait.....</div>}>
            <LazyAbout />
          </Suspense>
        } />
        <Route path="/Product/:id" element={<ProductDetails />}></Route>
        <Route path="Employee" element={<EmpListing />} />
        <Route path="Employee/Create" element={<EmpCreate />} />
        <Route path="Employee/Detail/:empid" element={<EmpDetail />} />
        <Route path="Employee/Edit/:empid" element={<EmpEdit />} />

        <Route path="/Cart" element={<Cart />} />
        <Route path="/Registration" element={<Registration />} />
        <Route path="/" element={<Login />} />
        <Route path="Payment" element={<Payment />} />
      </Routes>

      <Footer />
      

    </>
  );
}

export default App;
