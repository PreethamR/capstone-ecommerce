import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { NavLink, useNavigate } from 'react-router-dom'
import { addCart, delCart } from '../redux/action'
import * as i from 'react-bootstrap-icons'
import { ToWords } from 'to-words';

const Cart = () => {
    const state = useSelector((state) => state.handleCart)
    const dispatch = useDispatch();

    const handleAdd = (prod) => {
        dispatch(addCart(prod));
    };

    const handleDel = (prod) => {
        dispatch(delCart(prod));
    };
    const usenavigate = useNavigate();
    useEffect(() => {
        let username = sessionStorage.getItem('username');
        if (username === '' || username === null) {
            usenavigate('/')
        }
    }, [])
    var Total = 0;
    const toWords = new ToWords();

    const prod = (prod) => {
        Total = Total + (prod.qty) * (prod.Price)
        let Amount = toWords.convert(Total, { currency: true });
        return (
            <></>
        )

    }

    const emptyCart = () => {
        return (
            <div className='container-fluid' style={{ backgroundColor: 'lightpink' }}>
                <div className='row'>
                    <h1 className='display-5 fw-bolder' style={{ textAlign: 'center', marginTop: '90px', marginBottom: '90px', paddingBottom: '90px', paddingTop: '90px' }}>
                        <p style={{ fontFamily: 'serif' }}> <i.EmojiNeutralFill style={{ width: '60px', height: '60px' }} />EMPTY CART</p>
                    </h1>
                </div>
            </div>
        )
    }

    const cartItem = (prod) => {
        return (

            <div className="row" style={{ backgroundColor: 'lightpink' }}>
                <div className="col-lg-4">
                    <img src={prod.Images} alt={prod.Name} style={{ height: "300px", width: "300px", paddingBottom: '40px', marginTop: '20px', paddingLeft: '30px' }} />
                </div>
                <div className="col-lg-4 fw-bold">
                    <h3 style={{ marginTop: '20px' }}><b>{prod.Name}</b></h3>
                    <p style={{ fontSize: '30px' }}>
                        {prod.qty} X {prod.Price} = Rs {prod.qty * prod.Price}/-
                    </p>
                    <button className="btn btn-danger me-4" onClick={() => handleDel(prod)}>
                        <b><i.Dash style={{ height: '40px', width: '40px' }} /></b>
                    </button>
                    <button className="btn btn-success me-4" onClick={() => handleAdd(prod)}>
                        <i.Plus style={{ height: '40px', width: '40px' }} />
                    </button>

                </div>
            </div>
        )

    }

    const handleTotal = () => {
        return (

            <div className="display-6 fw-bold text-left" style={{ backgroundColor: 'lightpink' }}>
                {state.map(prod)}
                <div style={{ paddingBottom: '50px' }}>Total price = Rs.<span>{Total}/-</span></div>
                <div>Amount in words = <span>{toWords.convert(Total, { currency: true })}</span></div>
            </div>
        )

    }

    const button = () => {
        return (
            <div className='container-fluid' style={{ backgroundColor: 'lightpink' }}>
                <div className='button mx-5 px-5'>
                    <NavLink to='/Payment' className='btn btn-success fw-bolder mt-5 mb-5'>Process Payment</NavLink>
                </div>
            </div>
        )
    }
    return (
        <div>

            {state.length === 0 && emptyCart()}
            {state.length !== 0 && state.map(cartItem)}
            {state.length !== 0 && handleTotal()}
            {state.length !== 0 && button()}

        </div>
    )
}

export default Cart