import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { addCart } from '../redux/action/index';
import { useParams } from 'react-router';
import { NavLink } from 'react-router-dom';
import axios from 'axios';
import * as i from 'react-bootstrap-icons'

const ProductDetails = () => {

    const { id } = useParams();
    const [prod, setData] = useState([]);
    useEffect(() => {
        axios.get(`http://localhost:8000/Product/${id}`)
            .then(resp => setData(resp.data))

    }, [])

    const dispatch = useDispatch();
    const addProduct = (product) => {
        dispatch(addCart(product));
    }
    return (
        <>
        <div className='container-fluid' style={{height:'450px',backgroundColor:'lightblue'}}>
        <h1 className="fw-bolder text-align-center" style={{color:'Darkblue',textAlign:'center',textShadow:'2px 2px white',textDecoration:'underline'}}>More Info</h1>
            <div style={{ margin: '20px', float: 'left', paddingRight: '50px' }}>
                <img src={prod.Images}  style={{ width: '350px', height: '350px', paddingTop: '5px',borderRadius: '25px' }} />
            </div>
            <div>
                <h1 className="display-6 fw-bold" style={{ textDecoration: 'underline', marginTop: '30px',color:'royalblue' }}>{prod.Name}</h1>
                <p className="lead fw-bold py-4">
                    {prod.Description}
                </p>
                <h3 className="display-6 fw-bold my-4" >
                    Rs <span>{prod.Price}</span>/-
                </h3>
                <div style={{ marginBottom: '100px', marginTop: '50px' }}>
                    <button className="btn btn-success px-4 py-2" onClick={() => addProduct(prod)}><b>Add to Cart <i.CartPlus style={{ height: '25px', width: '25px' }}/></b></button>
                    <NavLink to="/cart" className="btn btn-primary ms-2 px-3 py-2"><b>Go to Cart <i.ArrowRightCircleFill style={{ height: '25px', width: '25px' }}/></b></NavLink>
                    </div>
                </div>
            </div>
            
        </>
    )
}

export default ProductDetails;





