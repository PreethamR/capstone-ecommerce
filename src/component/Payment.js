import React from 'react'
import { NavLink } from 'react-router-dom'

export default function () {
    return (
        <>
            <div style={{textAlign:'center'}}>
                <h3 className=' fw-bold'>Payment page</h3>
                <p className=' fw-bold'>Please make the payment</p>
                <h4 className='fw-bold'>Thank you for Ordering from our Store!! </h4>
                <NavLink to="/Home" className='btn btn-success my-4'>Home</NavLink>
            </div>
        </>
    )
}
