import { useEffect, useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import * as i from 'react-bootstrap-icons'

const Login = () => {
  const [username, usernameupdate] = useState('');
  const [password, passwordupdate] = useState('');

  const usenavigate = useNavigate();

  useEffect(() => {
    sessionStorage.clear();
  }, []);

  const ProceedLogin = (e) => {
    e.preventDefault();
    if (validate()) {
      fetch("http://localhost:8000/user/" + username).then((res) => {
        return res.json();
      }).then((resp) => {

        if (Object.keys(resp).length === 0) {
          toast.error('Please Enter valid username');
        } else {
          if (resp.password === password) {
            toast.success('Success');
            sessionStorage.setItem('username', username);
            sessionStorage.setItem('userrole', resp.role);
            usenavigate('/Home')
          } else {
            toast.error('Please Enter valid credentials');
          }
        }
      }).catch((err) => {
        toast.error('Login Failed due to :' + err.message);
      });
    }
  }

  const ProceedLoginusingAPI = (e) => {
    e.preventDefault();
    if (validate()) {
      let inputobj = {
        "username": username,
        "password": password
      };
      fetch("https://localhost:44308/User/Authenticate", {
        method: 'POST',
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify(inputobj)
      }).then((res) => {
        return res.json();
      }).then((resp) => {
        console.log(resp)
        if (Object.keys(resp).length === 0) {
          toast.error('Login failed, invalid credentials');
        } else {
          toast.success('Success');
          sessionStorage.setItem('username', username);
          sessionStorage.setItem('jwttoken', resp.jwtToken);
          usenavigate('/')
        }
      }).catch((err) => {
        toast.error('Login Failed due to :' + err.message);
      });
    }
  }
  const validate = () => {
    let result = true;
    if (username === '' || username === null) {
      result = false;
      toast.warning('Please Enter Username');
    }
    if (password === '' || password === null) {
      result = false;
      toast.warning('Please Enter Password');
    }
    return result;
  }
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light bg-warning fixed-top shadow-sm">
        
        <div className="container-fluid">
          <NavLink className="navbar-brand fw-bold fs-5" to="/">
            <img src='/Images/logo.png' alt='Logo' height={80} width={80} to='/' />
            COOL BITES</NavLink>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
        </div>
      </nav>


      <form onSubmit={ProceedLogin} className="container-fluid bg-info">
        <h2 className="fw-bold text-white" style={{textAlign:'center'}}>Welcome to Our Page</h2>
        <h3 className="fw-bold" style={{textAlign:'center'}}>Please Login to Continue Further</h3>
        <div className="row">
          <div className="offset-lg-3 col-lg-6 py-3">
            <div className="card">
              <div className="card-header bg-dark-subtle">
                <h2 className="text-primary" style={{ textAlign: 'center' }}><b>USER LOGIN</b></h2>
              </div>
              <div className="card-body bg-info-subtle">
                <div className="form-group my-3">
                  <label className="abc my-1">< i.PeopleFill style={{ height: '30px', width: '30px' }} />   <b>USER NAME <span className="errmsg">*</span></b></label>
                  <input value={username} onChange={e => usernameupdate(e.target.value)} className="form-control"></input>
                </div>
                <div className="form-group my-3">
                  <label className="abc my-1"><i.LockFill style={{ height: '30px', width: '30px' }} /><b> PASSWORD <span className="errmsg">*</span></b></label>
                  <input type="password" value={password} onChange={e => passwordupdate(e.target.value)} className="form-control"></input>
                </div>
              </div>
              <div className="card-footer bg-dark-subtle">
                <button type="submit " className="btn btn-primary mx-2"><b>LOGIN <i.PersonFill style={{ height: '25px', width: '25px' }} /></b></button>
                <NavLink className="btn btn-success mx-2" to={'/Registration'}> <b>NEW USER <i.PersonFillAdd style={{ height: '25px', width: '25px' }} /></b></NavLink>
              </div>
            </div>
          </div>
        </div>
      </form>
    </>
  );
}

export default Login;