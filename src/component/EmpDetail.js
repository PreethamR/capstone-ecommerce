import { useEffect, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
import * as i from 'react-bootstrap-icons'

const EmpDetail = () => {
    const { empid } = useParams();

    const [empdata, empdatachange] = useState({});

    useEffect(() => {
        fetch("http://localhost:8000/employee/" + empid).then((res) => {
            return res.json();
        }).then((resp) => {
            empdatachange(resp);
        }).catch((err) => {
            console.log(err.message);
        })
    }, []);
    return (
            <div className="container my-5 ">

                <div className="card row bg-danger-subtle">
                    <div className="card-title">
                        <h2 style={{fontWeight:'bold',textAlign:'center',paddingTop:'20px',color:'red',textDecoration:'underline'}}>Employee Details</h2>
                    </div>
                    <div className="card-body"></div>

                    {empdata &&
                        <div>
                            <h2> <i.PersonFill /> The Employee Name is :<b style={{color:'red'}}> {empdata.Name}</b> </h2>
                            <h2> <i.CheckCircleFill /> With Employee Id as:<b style={{color:'red'}}> {empdata.id}</b></h2>
                            <h2><b style={{color:'red',textDecoration:'underline dotted'}}>Employee Details:</b></h2><br/>
                            <h5><b style={{color:'red'}}><i.TelephoneFill /> Phone Number:  </b> <b>{empdata.Phone}</b> </h5>
                            <h5><b style={{color:'red'}}><i.HourglassSplit /> Designation:   </b> <b>{empdata.Designation}</b></h5>
                            <h5><b style={{color:'red'}}><i.SendExclamationFill /> Experience:    </b> <b>{empdata.Experience}</b></h5>
                            <h5><b style={{color:'red'}}><i.CurrencyRupee /> Salary:        </b> <b>{empdata.Salary}</b></h5>
                            <NavLink className="btn btn-danger mb-4" to="/Employee"> <i.SkipBackward/> Back to the List</NavLink>
                        </div>
                    }
                </div>
            </div >
       );
}

export default EmpDetail;
