import { useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

import * as i from 'react-bootstrap-icons'

const EmpCreate = () => {

    const [id, idchange] = useState();
    const [Name, namechange] = useState("");
    const [Phone, phonechange] = useState("");
    const [Designation, descchange] = useState("");
    const [Experience, expchange] = useState("");
    const [Salary, salarychange] = useState("");
    // const [active, activechange] = useState(true);
    const [validation, valchange] = useState(false);


    const navigate = useNavigate();

    const handlesubmit = (e) => {
        e.preventDefault();
        const empdata = { Name, Phone, Designation, Experience, Salary };


        fetch("http://localhost:8000/employee", {
            method: "POST",
            headers: { "content-type": "application/json" },
            body: JSON.stringify(empdata)
        }).then((res) => {
            toast.success('Saved successfully.')
            navigate('/Employee');
        }).catch((err) => {
            console.log(err.message)
        })

    }

    return (
        <>

            <div className="row py-4 bg-success" >
                <div className="offset-lg-3 col-lg-6 bg-success-subtle" style={{ boxShadow: '2px 2px 7px 2px black' }}>
                    <form className="container my-2" onSubmit={handlesubmit} >

                        <div className="card bg-success-subtle" style={{ "textAlign": "left" }}>
                            <div className="card-title bg-success-subtle">
                                <h2 style={{ fontWeight: 'bold', textAlign: 'center', paddingTop: '20px',color:'darkgreen',textDecoration:'underline' }}>Employee Creation</h2>

                                <div className="card-body bg-success-subtle">

                                    <div className="row fw-bold">

                                        <div className="col-lg-12">
                                            <div className="form-group">
                                                <label><i.CheckCircleFill/> ID</label>
                                                <input value={id} disabled="disabled" className="form-control"></input>
                                            </div>
                                        </div>

                                        <div className="col-lg-12">
                                            <div className="form-group">
                                                <label><i.PersonFill/> Name</label>
                                                <input required value={Name} onMouseDown={e => valchange(true)} onChange={e => namechange(e.target.value)} className="form-control"></input>
                                                {Name.length == 0 && validation && <span className="text-danger">Enter the Name</span>}
                                            </div>
                                        </div>
                                        <div className="col-lg-12">
                                            <div className="form-group">
                                                <label><i.TelephoneFill/> Phone</label>
                                                <input value={Phone} onChange={e => phonechange(e.target.value)} className="form-control"></input>
                                            </div>
                                        </div>

                                        <div className="col-lg-12">
                                            <div className="form-group">
                                                <label><i.HourglassSplit/> Designation</label>
                                                <input value={Designation} onChange={e => descchange(e.target.value)} className="form-control"></input>
                                            </div>
                                        </div>
                                        <div className="col-lg-12">
                                            <div className="form-group">
                                                <label><i.SendExclamationFill/> Experience</label>
                                                <input value={Experience} onChange={e => expchange(e.target.value)} className="form-control"></input>
                                            </div>
                                        </div>
                                        <div className="col-lg-12">
                                            <div className="form-group">
                                                <label><i.CurrencyRupee/> Salary</label>
                                                <input value={Salary} onChange={e => salarychange(e.target.value)} className="form-control"></input>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-12 my-3">
                                        <div className="form-group">
                                            <button className="btn btn-outline-success mx-2" type="submit"><i.Save /> Save </button>
                                            <NavLink to="/Employee"><button className="btn btn-outline-danger mx-2"><i.Back /> Back</button></NavLink>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </>
    );
}

export default EmpCreate;
