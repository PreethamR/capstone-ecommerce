import React from 'react'
import { NavLink } from 'react-router-dom'
import * as i from 'react-bootstrap-icons'
import { useSelector } from 'react-redux'

function Header() {
    const state = useSelector((state) => state.handleCart)
    return (
        <>
            <nav className="navbar navbar-expand-lg navbar-light bg-warning shadow-sm">
                <div className="container-fluid">
                    <NavLink className="navbar-brand fw-bold fs-5" to="/">
                        <img src='/Images/logo.png' alt='Logo' height={80} width={80} to='/' />
                        COOL BITES</NavLink>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className='buttons'>
                        <div className="collapse navbar-collapse">
                            <ul className="navbar-nav mx-auto mb-2 mb-lg-0">
                                <li className="nav-item">
                                    <NavLink className="btn btn-outline-dark ms-2 fs-l fw-bold" aria-current="page" to="/Home">
                                        HOME</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className="btn btn-outline-dark ms-2 fs-l fw-bold" to="/Product">
                                        PRODUCT</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className="btn btn-outline-dark ms-2 fs-l fw-bold" to="/Employee">
                                        EMPLOYEE</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className="btn btn-outline-dark ms-2 fs-l fw-bold" to="/About">ABOUT US
                                    </NavLink>
                                </li>
                            </ul>
                            <NavLink to="/cart" className="btn btn-outline-dark ms-2 fs-l fw-bold">
                                <i className="Cart me-1"></i>CART ({state.length}) <i.Cart4 /></NavLink>
                            <NavLink to="/" className="btn btn-outline-dark ms-2 fs-l fw-bold">
                                <i className="Cart me-1"></i>LOG OUT <i.BoxArrowRight /></NavLink>
                        </div>
                    </div>
                </div>
            </nav>
        </>


    )
}

export default Header