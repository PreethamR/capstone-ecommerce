import { useEffect, useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import * as i from 'react-bootstrap-icons'

const EmpListing = () => {
    const [empdata, empdatachange] = useState('');
    const navigate = useNavigate();

    const LoadDetail = (id) => {
        navigate("/Employee/Detail/" + id);
    }
    const LoadEdit = (id) => {
        navigate("/Employee/Edit/" + id);
    }
    const Removefunction = (id) => {
        if (window.confirm('Do you want to Delete this Employee :' + id)) {
            fetch("http://localhost:8000/employee/" + id, {
                method: "DELETE"
            }).then((res) => {
                alert("Deleted successfully :" + id)
                window.location.reload();
            }).catch((err) => {
                console.log(err.message)
            })
        }
    }




    useEffect(() => {
        fetch("http://localhost:8000/employee").then((res) => {
            return res.json();
        }).then((resp) => {
            empdatachange(resp);
        }).catch((err) => {
            console.log(err.message);
        })

        let username = sessionStorage.getItem('username');
        if (username === '' || username === null) {
            navigate('/')
        }

    }, [])
    return (
        <div className="card bg-warning-subtle">
            <div className="container mx-5 my-5 bg-warning-subtle">

                <div className="card-title">
                    <h2 style={{ fontWeight: 'bold', textAlign: 'center', paddingTop: '20px', textDecoration: 'underline' }}>Employee Information</h2>
                </div>
                <div className="card-body bg-warning-subtle">
                    <div className="divbtn">
                        <NavLink to="/Employee/Create" className="btn btn-success"><i.PersonPlusFill /> Add New </NavLink><br /><br />
                    </div>
                    <table className="table table-bordered table-striped">
                        <thead className="bg-dark text-white fw-bolder">
                            <tr style={{ textAlign: 'center' }}>
                                <td>ID</td>
                                <td>Name</td>
                                <td>Phone</td>
                                <td>Designation</td>
                                <td>Experience</td>
                                <td>Salary</td>
                                <td>Action</td>

                            </tr>
                        </thead>
                        <tbody>

                            {empdata &&
                                empdata.map(item => (
                                    <tr key={item.id}>
                                        <td>{item.id}</td>
                                        <td> {item.Name}</td>
                                        <td> {item.Phone}</td>
                                        <td>{item.Designation}</td>
                                        <td>{item.Experience}</td>
                                        <td>{item.Salary}</td>
                                        <td><a onClick={() => { LoadEdit(item.id) }} className="btn btn-warning mx-2"> Edit <i.Eyedropper /></a>
                                            <a onClick={() => { Removefunction(item.id) }} className="btn btn-danger mx-2"> Delete <i.Trash /></a>
                                            <a onClick={() => { LoadDetail(item.id) }} className="btn btn-primary mx-2"> Details <i.ArrowBarRight /></a>
                                        </td >
                                    </tr >
                                ))}

                        </tbody >
                    </table >
                </div >
            </div >
        </div >
    );
}

export default EmpListing;
