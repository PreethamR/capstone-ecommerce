import React from 'react'
import * as i from 'react-bootstrap-icons'

function About() {
  return (
    <>
      <div><h2 style={{ textAlign: 'center', marginTop: '10px',color:'orange',textShadow:'2px 2px black',fontWeight:'bold' }}><u>ABOUT US</u></h2></div>
      <p style={{ margin: '20px', padding: '25px', fontSize: '20px', backgroundColor: 'lightyellow' }}>
        The Cool BITES team is a diverse group of people working closely together each day to provide desserts that help people celebrate the joysin their own lives and the lives of others.
        To share love and good wishes.
        And to bring a bit of comfort and happiness to those who deserve it.
        Our experienced bakers mix all of our batters and frostings by hand, just like grandma used to make.  We always bake with whole buttermilk, fresh eggs and real butter – never a cake mix or commercially made frostings.
        We love what we do, take pride in each and every cake we put our name on, and guarantee your complete satisfaction.
        We live our mission everyday to help people celebrate, commemorate, appreciate, and send a message of love through the joy of cake.
        We look forward to playing a small part in the important times in your lives.<br /><br />
        <h4>Contact us on </h4>
        <i.Telephone className=' ms-3' style={{ width: '50px', height: '50px' }} />
        <i.Whatsapp className=' ms-3' style={{ width: '50px', height: '50px' }} />
        <i.Instagram className=' ms-3' style={{ width: '50px', height: '50px' }} />
        <i.Telegram className='telegram ms-3' style={{ width: '50px', height: '50px' }} /><br /><br />
        <h4>Direct Contact for Ordering</h4>
        <p> +91 1234567890</p>

      </p>

    </>
  )
}

export default About