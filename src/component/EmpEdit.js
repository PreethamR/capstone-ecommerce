import { useEffect, useState } from "react";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import * as i from 'react-bootstrap-icons'

const EmpEdit = () => {
    const { empid } = useParams();
    useEffect(() => {
        fetch("http://localhost:8000/employee/" + empid)
            .then((res) => {
                return res.json();
            }).then((resp) => {
                idchange(resp.id);
                Namechange(resp.Name);
                Phonechange(resp.Phone);
                Designationchange(resp.Designation);
                Experiencechange(resp.Experience);
                Salarychange(resp.Salary);
            }).catch((err) => {
                console.log(err.message);
            })
    }, []);

    const [id, idchange] = useState("");
    const [Name, Namechange] = useState("");
    const [Phone, Phonechange] = useState("");
    const [Designation, Designationchange] = useState("");
    const [Experience, Experiencechange] = useState("");
    const [Salary, Salarychange] = useState("");
    const [validation, valchange] = useState(false);


    const navigate = useNavigate();

    const handlesubmit = (e) => {
        e.preventDefault();
        const empdata = { id, Name, Phone, Designation, Experience, Salary };


        fetch("http://localhost:8000/employee/" + empid, {
            method: "PUT",
            headers: { "content-type": "application/json" },
            body: JSON.stringify(empdata)
        }).then((res) => {
            toast.success('Saved successfully.')
            navigate('/Employee');
        }).catch((err) => {
            console.log(err.message)
        })

    }
    return (
        <>

            <div className="row py-4" style={{ backgroundColor: 'lightskyblue' }}>
                <div className="offset-lg-3 col-lg-6" style={{ boxShadow: '2px 2px 7px 2px black', backgroundColor: 'lightskyblue' }}>
                    <form className="container my-1" onSubmit={handlesubmit}>

                        <div className="card " style={{ textAlign: "left", backgroundColor: 'lightskyblue' }}>
                            <div className="card-title bg-primary-subtle" style={{ fontWeight: 'bold', paddingTop: '20px' }}>
                                <h2 style={{ fontWeight: 'bold', textAlign: 'center', paddingTop: '20px', color: 'darkblue', textDecoration: 'underline' }} >Edit Employee Details</h2>

                                <div className="card-body">

                                    <div className="row fw-bold">

                                        <div className="col-lg-12">
                                            <div className="form-group">
                                                <label><i.CheckCircleFill /> ID</label>
                                                <input value={id} disabled="disabled" className="form-control"></input>
                                            </div>
                                        </div>

                                        <div className="col-lg-12">
                                            <div className="form-group">
                                                <label><i.PersonFill /> Name</label>
                                                <input required value={Name} onMouseDown={e => valchange(true)} onChange={e => Namechange(e.target.value)} className="form-control"></input>
                                                {Name.length == 0 && validation && <span className="text-danger">Enter the Name</span>}
                                            </div>
                                        </div>

                                        <div className="col-lg-12">
                                            <div className="form-group">
                                                <label><i.TelephoneFill /> Phone</label>
                                                <input value={Phone} onChange={e => Phonechange(e.target.value)} className="form-control"></input>
                                            </div>
                                        </div>


                                        <div className="col-lg-12">
                                            <div className="form-group">
                                                <label><i.HourglassSplit /> Designation</label>
                                                <input value={Designation} onChange={e => Designationchange(e.target.value)} className="form-control"></input>
                                            </div>
                                        </div>

                                        <div className="col-lg-12">
                                            <div className="form-group">
                                                <label><i.SendExclamationFill /> Experience</label>
                                                <input value={Experience} onChange={e => Experiencechange(e.target.value)} className="form-control"></input>
                                            </div>
                                        </div>

                                        <div className="col-lg-12">
                                            <div className="form-group">
                                                <label><i.CurrencyRupee /> Salary</label>
                                                <input value={Salary} onChange={e => Salarychange(e.target.value)} className="form-control"></input>
                                            </div>
                                        </div>

                                        <div className="col-lg-12 my-3 ">
                                            <div className="form-group">
                                                <button className="btn btn-outline-primary mx-2" type="submit">Update <i.Upload/></button>
                                                <NavLink to="/Employee"><button className="btn btn-outline-danger mx-2"><i.SkipBackward/>  Back </button></NavLink>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </>
    );
}
export default EmpEdit;
