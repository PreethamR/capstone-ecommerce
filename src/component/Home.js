import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'

function Home() {
  const usenavigate = useNavigate();
  useEffect(() => {
    let username = sessionStorage.getItem('username');
    if (username === '' || username === null) {
      usenavigate('/')
    }
  }, [])
  return (
    <>
      <h4 className='abc fw-bolder fs-xxl text-dark' style={{ textAlign: 'center', textShadow: '2px 2px 2px gold' }}> WELCOME TO THE COOL BITES SHOPPING PAGE</h4>

      <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner" style={{ margin: '20px' }}>
          <div class="carousel-item ">
            <img src="Images/Ice Cream 1.jpg" class="d-block w-80" width="1200" height="500" />
            <div class="carousel-caption text-light">
              <h2><strong>TRADITIONAL ICE CREAM</strong></h2>
              <h4><strong>Since 1980</strong></h4>
              <p>Traditional and Delicious</p>
            </div>
          </div>

          <div class="carousel-item active">
            <img src="Images/ice2.jpeg" class="d-block w-80" width="1200" height="500" />
            <div class="carousel-caption">
              <h2><strong> WELCOME TO CAKEZONE</strong></h2>
              <h4><strong>Unforgettable Taste</strong></h4>
              <p>The Best Cake in Bengaluru</p>
            </div>
          </div>

          <div class="carousel-item">
            <img src="Images/ice3.jpg" class="d-block w-80" width="1200" height="500" />
            <div class="carousel-caption text-white">
              <h2><strong>TASTY & FRESH</strong></h2>
              <h4><strong>
                <p>One step to making a good start</p>
              </strong></h4>
              <p>Life is like Ice Cream, Enjoy it Before it Melts</p>
            </div>
          </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>


        <div className="row justify-content-lg-center ms-5 me-5 mt-5" style={{}} >
          <div className="card  ms-3" style={{ float: 'left', width: '350px', borderBlock: '5px solid', borderBlockColor: 'red' }} >
            <img src="Images/ban6.png" className="card-img-top" />
            <div className="card-body">
              <h5 className="card-title">Upto 50% OFF on Selected Products</h5>
            </div>
          </div>


          <div className="card  ms-3" style={{ float: 'left', width: '350px', borderBlock: '5px solid', borderBlockColor: 'red' }}>
            <img src="Images/ban7.jpg" className="card-img-top" />
            <div className="card-body">
              <h5 className="card-title">Special Discount on purchase of above 1000 Get upto 25% off</h5>
            </div>
          </div>

          <div className="card ms-3" style={{ float: 'left', width: '350px', borderBlock: '5px solid', borderBlockColor: 'red' }}>
            <img src="Images/ban5.jpg" className="card-img-top" />
            <div className="card-body">
              <h5 className="card-title">Buy 1 and Get 1 Free</h5>
            </div>
          </div>
        </div>
      </div>
      <br />

    </>
  )
}

export default Home