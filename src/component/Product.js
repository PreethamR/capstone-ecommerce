import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { NavLink, Outlet, useNavigate } from 'react-router-dom'
import { addCart } from '../redux/action';
import * as i from 'react-bootstrap-icons'

const Product = () => {
    const dispatch = useDispatch();
    const addProduct = (product) => {
        dispatch(addCart(product));
    }

    const [data, setData] = useState([])
    const usenavigate = useNavigate();
    useEffect(() => {
        axios.get(`http://localhost:8000/Product`)
            .then(resp => setData(resp.data))
        let username = sessionStorage.getItem('username');
        if (username === '' || username === null) {
            usenavigate('/')
        }
    }, [])

    const [filter, setfilter] = useState('');
    const searchtext = (event) => {
        setfilter(event.target.value);
    }

    let datasearch = data.filter(prod => {
        return Object.keys(prod).some(key =>
            prod[key].toString().toLowerCase().includes(filter.toString().toLowerCase())
        )
    }
    );

    return (
        <>
            <div className='fs-1 fw-bold text-dark ' style={{ textAlign: 'center', textShadow: '2px 2px white', backgroundColor: 'turquoise' }}><u>Product List</u></div>
            <section className='py-4 container-fluid' style={{ backgroundColor: 'turquoise' }} >
                <div className='row justify-content-center'>

                    <div className='col-12 mb-2'>
                        <div className='mb-3 col-4 mx-auto text-center'>
                            <label className='form-label h5' /><b>Search for the Items</b>
                            <input
                                type="text "
                                className='form-control'
                                value={filter}
                                onChange={searchtext.bind(this)}/>
                                
                        </div>
                    </div>

                    {datasearch.map((prod) => {
                        return (
                            <div className="card bg-danger-subtle" key={prod.id} style={{ width: '400px', height: '550px', margin: '5px', float: 'left', alignItems: 'center', marginBottom: '20px' }}>
                                <h3 className="card-title text-danger fw-bolder" style={{ textAlign: 'center', textShadow: '2px 2px white' }}> {prod.Name}</h3>
                                <img src={prod.Images} className="card-img-top" style={{ width: '250px', height: '250px', paddingTop: '5px', borderRadius: '25px' }}></img>
                                <h5 className="card-title text-dark"> Id: {prod.id}</h5>
                                <div className="card-body bg-danger-subtle">
                                    <h5 className="card-title text-dark"> Price: Rs {prod.Price} /-</h5>
                                    <p className="card-text">{prod.Description}</p>
                                    <nav>
                                        <NavLink to={`/Product/${prod.id}`} className="btn btn-warning ms-3 me-1"><b>More Info<i.InfoLg style={{ height: '25px', width: '25px' }}/></b></NavLink>
                                        <button className="btn btn-primary ms-3 me-1 " onClick={() => addProduct(prod)}><b>Add to Cart <i.CartCheckFill style={{ height: '25px', width: '25px' }}/></b></button>

                                    </nav>
                                    <Outlet />
                                </div>
                            </div>
                        )
                    })}
                </div>
            </section>
        </>
    )
}
export default Product