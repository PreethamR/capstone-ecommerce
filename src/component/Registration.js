import { useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import * as i from 'react-bootstrap-icons'

const Registration = () => {

  const [id, idchange] = useState("");
  const [name, namechange] = useState("");
  const [password, passwordchange] = useState("");
  const [email, emailchange] = useState("");
  const [phone, phonechange] = useState("");
  const [country, countrychange] = useState("india");
  const [address, addresschange] = useState("");
  const [gender, genderchange] = useState("male");

  const navigate = useNavigate();

  const IsValidate = () => {
    let isproceed = true;
    let errormessage = 'Please enter the value in ';
    if (id === null || id === '') {
      isproceed = false;
      errormessage += ' Username';
    }
    if (name === null || name === '') {
      isproceed = false;
      errormessage += ' Fullname';
    }
    if (password === null || password === '') {
      isproceed = false;
      errormessage += ' Password';
    }
    if (email === null || email === '') {
      isproceed = false;
      errormessage += ' Email';
    }

    if (!isproceed) {
      toast.error(errormessage)
    } else {
      if (/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(email)) {

      } else {
        isproceed = false;
        toast.error('Please enter the valid email')
      }
    }
    return isproceed;
  }


  const handlesubmit = (e) => {
    e.preventDefault();
    let regobj = { id, name, password, email, phone, country, address, gender };
    if (IsValidate()) {
      fetch("http://localhost:8000/user", {
        method: "POST",
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify(regobj)
      }).then((res) => {
        toast.success('Registered successfully.')
        navigate('/');
      }).catch((err) => {
        toast.error('Failed :' + err.message);
      });
    }
  }
  return (
    <>
      <form className="container-fluid bg-danger py-4" onSubmit={handlesubmit}>
        <div className="offset-lg-3 col-lg-6">
          <div className="card">
            <div className="card-header bg-danger-subtle">
              <h1 style={{ textAlign: 'center',color:'red' }}><b>User Registeration</b></h1>
            </div>
            <div className="card-body bg-warning-subtle">

              <div className="row">
                <div className="col-lg-6">
                  <div className="form-group my-2">
                    <label><i.PersonFill style={{ height: '25px', width: '25px' }} /> <b>USER NAME <span className="errmsg">*</span></b></label>
                    <input value={id} onChange={e => idchange(e.target.value)} className="form-control"></input>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form-group my-2">
                    <label><i.LockFill style={{ height: '25px', width: '25px' }} /><b> PASSWORD <span className="errmsg">*</span></b></label>
                    <input value={password} onChange={e => passwordchange(e.target.value)} type="password" className="form-control"></input>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form-group my-2">
                    <label><i.PersonFillCheck style={{ height: '25px', width: '25px' }} /><b>FULL NAME <span className="errmsg">*</span ></b></label>
                    <input value={name} onChange={e => namechange(e.target.value)} className="form-control"></input>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form-group my-2">
                    <label><i.Mailbox2 style={{ height: '25px', width: '25px' }} /><b>EMAIL ID <span className="errmsg">*</span></b> </label>
                    <input value={email} onChange={e => emailchange(e.target.value)} className="form-control"></input>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form-group my-2">
                    <label><i.PhoneFill style={{ height: '25px', width: '25px' }} /> <b>MOBILE NUMBER <span className="errmsg"></span></b></label>
                    <input value={phone} onChange={e => phonechange(e.target.value)} className="form-control"></input>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form-group my-2">
                    <label> <i.PinMapFill style={{ height: '25px', width: '25px' }} /> <b>COUNTRY <span className="errmsg">*</span></b></label>
                    <select value={country} onChange={e => countrychange(e.target.value)} className="form-control">
                      <option value="india">India</option>
                      <option value="russia">Russia</option>
                      <option value="germany">Germany</option>
                      <option value="usa">USA</option>
                      <option value="singapore">Singapore</option>
                    </select>
                  </div>
                </div>
                <div className="col-lg-12">
                  <div className="form-group my-2">
                    <label><i.DatabaseAdd style={{ height: '25px', width: '25px' }} /> <b>ADDRESS</b></label>
                    <textarea value={address} onChange={e => addresschange(e.target.value)} className="form-control"></textarea>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form-group my-2">
                    <label><i.PeopleFill style={{ height: '25px', width: '25px' }} /><b>Gender</b> </label>
                    <br></br>
                    <input type="radio" checked={gender === 'male'} onChange={e => genderchange(e.target.value)} name="gender" value="male" className="app-check"></input>
                    <label className="mx-3"><b><i.GenderMale style={{ height: '25px', width: '25px' }} />MALE</b> </label>
                    <input type="radio" checked={gender === 'female'} onChange={e => genderchange(e.target.value)} name="gender" value="female" className="app-check"></input>
                    <label className="mx-3"><b><i.GenderFemale style={{ height: '25px', width: '25px' }} /> FEMALE</b> </label>
                  </div>
                </div>

              </div>

            </div>
            <div className="card-footer bg-danger-subtle">
              <button type="submit" className="btn btn-primary mx-2"><b>REGISTER <i.ArrowRight style={{ height: '25px', width: '25px' }} /></b> </button>
              <NavLink to={'/'} className="btn btn-danger mx-2"><b>CLOSE<i.ArrowRepeat style={{ height: '25px', width: '25px' }} /></b>  </NavLink>
            </div>
          </div>
        </div >
      </form>
    </>
  );
}

export default Registration;
